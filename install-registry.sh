apt-get install -y ca-certificates librados2 apache2-utils
git clone https://github.com/docker/distribution-library-image.git
cd distribution-library-image
mkdir -p /etc/docker/registry/
mkdir /var/lib/registry
cp registry/registry /bin/registry
cp registry/config-example.yml /etc/docker/registry/config.yml
cd

#Create cert or download it to /certs/zdocker.crt
mkdir -p /certs && openssl req   -newkey rsa:4096 -nodes -sha256 -keyout /certs/zdocker.key   -x509 -days 365 -out /certs/zdocker.crt

#Create htpasswd
mkdir /auth
htpasswd -Bbn mrkhan1417 sarfarazali7 > /auth/htpasswd

export REGISTRY_AUTH=htpasswd
export REGISTRY_AUTH_HTPASSWD_REALM=Registry Realm
export REGISTRY_HTTP_TLS_KEY=/certs/zdocker.key
export REGISTRY_HTTP_TLS_CERTIFICATE=/certs/zdocker.crt
export REGISTRY_AUTH_HTPASSWD_PATH=/auth/htpasswd

registry /etc/docker/registry/config.yml &